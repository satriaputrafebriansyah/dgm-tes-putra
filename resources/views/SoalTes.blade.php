<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Soal Tes</title>
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <style>
            .flex-container {
                display: flex;
                flex-wrap: wrap;
                gap: 10px;
                padding-top : 20px;
            }
            .container {
                padding : 30px;
            }
        </style>
    </head>
    <body class="font-sans antialiased dark:bg-black dark:text-white/50">
        <main class="mt-6 container">

                <!-- SOAL NO.1 -->

                <h2>1. Sorted Array</h2>
                <div class="flex-container">
                    @foreach ($sortedArray as $item)
                        <p class="mt-4 text-sm/relaxed">
                            {{ $item }}
                        </p>
                    @endforeach
                </div>

                <!-- END SOAL NO.1 -->

                <!-- SOAL NO.2 -->

                <h2>2. Pattern Count</h2>
                <form id="submitPattern" action="{{ route('submit.pattern') }}" method="POST">
                    @csrf
                    <input type="text" name="text" placeholder="Input Text" value="{{ $text }}" required>
                    <input type="text" name="pattern" placeholder="Input Pattern" value="{{ $pattern }}">
                    <button type="submit">Submit</button>
                </form>
                <div class="flex-container">
                    <p id="patternCountList">Input: "{{ $text }}", "{{ $pattern ?? '' }}"</p>
                </div>
                <p id="patternCountResult"> Pattern Count: {{ $patternCount }}</p>

                <!-- END SOAL No.2 -->

                <!-- SOAL NO.3 -->

                <h2>3. Latter Count</h2>
                <form id="submitLetter" action="{{ route('submit.letter') }}" method="POST">
                    @csrf
                    <input type="text" name="inputText" placeholder="Input Text" value="{{ $inputText }}" required>
                    <button type="submit">Submit</button>
                </form>
                <p id="inputTextDisplay">Input: {{ $inputText }}</p>
                <div class="flex-container" id="letterCountList">
                    @foreach ($letterCount as $char => $count)
                        <p>"{{ $char }}": {{ $count }},</p>
                    @endforeach
                </div>

                <!-- END SOAL NO.3 -->
        </main>

        
        <!-------------------------- JS SOAL NO.2 ------------------------------>
         
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                document.getElementById('submitPattern').addEventListener('submit', function(event) {
                    event.preventDefault();
                    const formData = new FormData(this);

                    fetch(this.getAttribute('action'), {
                        method: 'POST',
                        body: formData
                    })
                    .then(response => response.json())
                    .then(data => {
                        console.log(data);

                        const patternText = data.pattern ? data.pattern : '""';

                        document.getElementById('patternCountList').innerHTML = `Input: "${data.text}", "${patternText}"`;
                        document.getElementById('patternCountResult').textContent = `Pattern Count: ${data.count}`;
                    })
                    .catch(error => console.error('Error:', error));
                });
            });
        </script>

        <!-------------------------- END JS SOAL NO.2 ------------------------------>

        <!-------------------------- JS SOAL NO.3 ---------------------------------->
         
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                document.getElementById('submitLetter').addEventListener('submit', function(event) {
                    event.preventDefault();
                    const formData = new FormData(this);
                    
                    fetch(this.getAttribute('action'), {
                        method: 'POST',
                        body: formData
                    })
                    .then(response => response.json())
                    .then(data => {
                        console.log(data);
                        const letterCountList = document.getElementById('letterCountList');
                        const inputTextDisplay = document.getElementById('inputTextDisplay');

                        if (letterCountList && inputTextDisplay) {
                            letterCountList.innerHTML = '';
                            inputTextDisplay.textContent = `Input: ${formData.get('inputText')}`;
                            for (const char in data) {
                                const count = data[char];
                                const listItem = document.createElement('p');
                                listItem.textContent = `"${char}": ${count},`;
                                letterCountList.appendChild(listItem);
                            }
                        } else {
                            console.error('Required elements not found in the DOM');
                        }
                    })
                    .catch(error => console.error('Error:', error));
                });
            });
        </script>

        <!-------------------------- END JS SOAL NO.3 ---------------------------------->

    </body>
</html>

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

Route::get('/', [HomeController::class, 'SoalTes']);
Route::post('/submitPattern', [HomeController::class, 'submitPattern'])->name('submit.pattern');
Route::post('/submitLetter', [HomeController::class, 'submitLetter'])->name('submit.letter');
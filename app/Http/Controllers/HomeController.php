<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    
    // -------------------------- RETURN VIEW ------------------------- //

    public function SoalTes(Request $request)
    {
        // Soal No.1
        $array = [12, 9, 30, "A", "M", 99, 82, "J", "N", "B"];
        $letters = array_filter($array, function($item) {
            return is_string($item);
        });

        $numbers = array_filter($array, function($item) {
            return is_int($item);
        });

        sort($letters);
        sort($numbers);
        $sortedArray = array_merge($letters, $numbers);

        // Soal No.2
        $text = $request->input('text', "aaaaaa");
        $pattern = $request->input('pattern', "aa");
        $patternCount = $this->pattern_count($text, $pattern);

        // Soal No.3
        $inputText = $request->input('inputText', 'MasyaAllah');
        $letterCount = $this->letter_count($inputText);

        // Hasil View
        return view('SoalTes', [
            'sortedArray' => $sortedArray,
            'patternCount' => $patternCount,
            'text' => $text,
            'pattern' => $pattern,
            'letterCount' => $letterCount,
            'inputText' => $inputText
        ]);
    }

    // -------------------------- END RETURN VIEW ------------------------- //

    // -------------------------- SOAL NO.2 ------------------------------ //

    public function submitPattern(Request $request)
    {
        $text = $request->input('text', '');
        $pattern = $request->input('pattern', '');
        $patternCount = $this->pattern_count($text, $pattern);

        return response()->json(['count' => $patternCount, 'text' => $text, 'pattern' => $pattern]);
    }

    public function pattern_count($text, $pattern)
    {
        $textLength = strlen($text);
        $patternLength = strlen($pattern);

        if ($patternLength === 0 || $textLength === 0 || $patternLength > $textLength) {
            return 0;
        }

        $count = 0;
        for ($i = 0; $i <= $textLength - $patternLength; $i++) {
            if (substr($text, $i, $patternLength) === $pattern) {
                $count++;
            }
        }

        return $count;
    }

    // -------------------------- END SOAL NO.2 -------------------------- //

    // -------------------------- SOAL NO.3 ------------------------------ //

    public function submitLetter(Request $request)
    {
        $inputText = $request->input('inputText', '');
        $letterCount = $this->letter_count($inputText);
 
        return response()->json($letterCount);
    }

     
    public function letter_count($text)
    {
        $count = [];
        for ($i = 0; $i < strlen($text); $i++) {
            $char = $text[$i];
            if (ctype_alpha($char)) {
                if (isset($count[$char])) {
                    $count[$char]++;
                } else {
                    $count[$char] = 1;
                }
            }
        }

        ksort($count);
        return $count;
    }

    // -------------------------- END SOAL NO.3 ------------------------------ //

}
